import { useState } from 'react';
import { listOfCards } from './data';

const MAX_VISIBLE_ITEMS = 3;
const SCALE_FACTOR = 0.2;
const SHIFT_FACTOR = 75;

function App() {
  const [items, setItems] = useState(listOfCards);

  function handlePrevClick() {
    const copyOfItems = items.slice();
    copyOfItems.unshift(copyOfItems.pop());
    setItems(copyOfItems);
  }

  function handleNextClick() {
    const copyOfItems = items.slice();
    copyOfItems.push(copyOfItems.shift());
    setItems(copyOfItems);
  }

  return (
    <main>
      <div className="container">
        {items.map((cartItem, index) => {
          const scaleBy = 1 - SCALE_FACTOR * index;
          const translateTo = SHIFT_FACTOR * index;
          const className = `card-item ${
            index < MAX_VISIBLE_ITEMS ? ' visible' : ''
          }`;

          const styles = {
            backgroundColor: cartItem.color,
            transform: `scale(${scaleBy}) translateX(${translateTo}%)`,
            zIndex: MAX_VISIBLE_ITEMS - index,
          };

          return (
            <div key={cartItem.title} className={className} style={styles}>
              {cartItem.title}
            </div>
          );
        })}
      </div>
      <div className="toolbar">
        <button onClick={handlePrevClick}>Prev</button>
        <button onClick={handleNextClick}>Next</button>
      </div>
    </main>
  );
}

export default App;
