export const listOfCards = [
  { title: 'CARD #1', color: 'green' },
  { title: 'CARD #2', color: 'brown' },
  { title: 'CARD #3', color: 'red' },
  { title: 'CARD #4', color: 'black' },
  { title: 'CARD #5', color: 'blue' },
  { title: 'CARD #6', color: 'purple' },
];
